#' Add SOM coordinates
#'
#' @param df a data frame to add coordinates to
#' @param nodeframe a nodeframe
#' @param by which column in df contains nodes
#'
#' @return df with additional columns x and y
#' @export
#'
add_coords = function(df, nodeframe, by = 'node') {
    df = left_join(df, nodeframe[, c('node', 'x', 'y')], by = c(by = 'node'))
    df
}
